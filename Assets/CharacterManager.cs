﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterManager : MonoBehaviour
{
    public Text overallLabel;
    public InputField nameLabel;
    public InputField levelLabel;
    Character character;

    private void Start()
    {
        
        GetCharacterStats();
    }
    public void GetCharacterStats()
    {
        if (gameObject.name == Game.current.paladin.job)
        {
            character = Game.current.paladin;
        }
        else if (name == Game.current.ninja.job)
        {
            character = Game.current.ninja;
        }
        else
        {
            character = Game.current.blackMage;
        }
        nameLabel.text = character.name;
        levelLabel.text = character.level.ToString();
        overallLabel.text = character.job + "\n" + character.name + " | Level: " + character.level.ToString();
    }

    public void SetCharacterStats()
    {
        character.name = nameLabel.text;
        character.level = Int32.Parse(levelLabel.text);
    }

}
