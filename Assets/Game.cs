﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Game
{

    public static Game current;
    public Character paladin;
    public Character ninja;
    public Character blackMage;

    public Game()
    {
        paladin = new Character("Paladin");
        ninja = new Character("Ninja");
        blackMage = new Character("Black Mage");
    }

}
