﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Dropdown savePicker;
    CharacterManager[] charManager;
    void Start()
    {
        Load();
        if (SaveLoad.savedGames.Count > 0)
        {
            Game.current = SaveLoad.savedGames[0];
            Debug.Log("Saves found, loading oldest one");
        }
        else
        {
            Game.current = new Game();
            Debug.Log("No Saves found, creating a new one");
        }
        charManager = FindObjectsOfType<CharacterManager>();
    }

    public void Save(bool newSave)
    {
        foreach (CharacterManager manager in charManager)
        {
            manager.SetCharacterStats();
        }
        SaveLoad.Save(newSave);
        foreach (CharacterManager manager in charManager)
        {
            manager.GetCharacterStats();
        }
        Load();
    }

    public void Load()
    {
        SaveLoad.Load();

        savePicker.ClearOptions();
        List<string> saveList = new List<string>();
        foreach (Game save in SaveLoad.savedGames)
        {
            saveList.Add(save.paladin.name + " | " + save.ninja.name + " | " + save.blackMage.name);
        }
        savePicker.AddOptions(saveList);


    }
    public void NewGame()
    {
        Game.current = new Game();
        foreach (CharacterManager manager in charManager)
        {
            manager.GetCharacterStats();
        }
    }
    public void OnPickerChange()
    {

        Game.current = SaveLoad.savedGames[savePicker.value];
        foreach (CharacterManager manager in charManager)
        {
            manager.GetCharacterStats();
        }
    }
}
