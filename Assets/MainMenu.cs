﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{

    public enum Menu
    {
        MainMenu
    }

    public Menu currentMenu;
    private bool toggle = false;

    private void Start()
    {
        Debug.Log(PlayerPrefs.GetInt("Vibrate"));
        toggle = PlayerPrefs.GetInt("Vibrate") == 1 ? true : false;
    }
    void OnGUI()
    {

        GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();

        if (currentMenu == Menu.MainMenu)
        {

            if (GUI.Button(new Rect(Screen.width - 190, Screen.height - 100, 50, 50),"Quit"))
            {
                Application.Quit();
            }
            toggle = GUI.Toggle(new Rect(Screen.width - 190, Screen.height - 160, 400, 400), toggle,
                                "Toggle Vibration");
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.EndArea();

    }

    void Update()
    {
        PlayerPrefs.SetInt("Vibrate", toggle == true ? 1 : 0);
        //1 == true
        //0 == false
        if (toggle == true && PlayerPrefs.GetInt("Vibrate") == 0)
        {
            PlayerPrefs.SetInt("Vibrate", 1);
        }
        else if (toggle == false && PlayerPrefs.GetInt("Vibrate") == 1)
        {
            PlayerPrefs.SetInt("Vibrate", 0);
        }

        if (toggle)
        {
            // Do something when it's toggled on
            Handheld.Vibrate();
        }
        Debug.Log(PlayerPrefs.GetInt("Vibrate"));
    }
}
