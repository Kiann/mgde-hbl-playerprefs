﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Character
{

    public string name;
    public string job;
    public int level;

    public Character(string Job)
    {
        this.name = "";
        this.job = Job;
        this.level = 1;
    }
}